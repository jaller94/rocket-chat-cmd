# Rocket Chat cmd

Post to Rocket chat from your command line.

## Configure
Copy `.env-sample` to `.env` and enter your login credentials.
Get a UserId and AuthToken from the user settings inside your RocketChat instance.
```bash
cp .env-sample .env
$VISUAL .env
```

## Run

You can post your standup check-in:
```bash
cat standup.txt | node index.txt --room "#standup"
```

Remind people to do their code reviews:
```bash
echo "There are 8 PRs waiting to be reviewed!" | node index.txt --room "#devs" --emoji ":cop:" --alias "PR Cop"
```
