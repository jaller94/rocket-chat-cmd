#!/usr/bin/env node
const argv = require('yargs')
    .usage('Usage: echo "hi people" | $0 [options]')
    .option('room', {
        describe: 'Room identifier (e.g. #PublicChannel, .PrivateGroup, @christian, or hA8WF8A4fvbrgjoEa)',
        alias: 'r',
        type: 'string',
    })
    .option('alias', {
        describe: 'Name to replace your display name (e.g. "PR Cop")',
        alias: 'a',
        type: 'string',
    })
    .option('emoji', {
        describe: 'Emoji short hand to replace your avatar (e.g. :cop:)',
        alias: 'e',
        type: 'string',
    })
    .demandOption(['r'])
    .argv;
require('dotenv').config();
const RocketChatApi = require('rocketchat-api');

process.stdin.setEncoding('utf8');
let message = '';

process.stdin.on('readable', () => {
    let chunk;
    // Use a loop to make sure we read all available data.
    while ((chunk = process.stdin.read()) !== null) {
        message += chunk; 
    }
});

process.stdin.on('end', () => {
    if (message.trim() === '') {
        console.error(`No message received via stdin.`);
        process.exit(2);
    }

    post(argv.room, message, argv.alias, argv.emoji).catch(console.error);
});

function requireEnv(name) {
    if (process.env[name]) return;
    console.error(`Please specify the env var ${name}.`);
    process.exit(1);
}

requireEnv('PROTOCOL');
requireEnv('HOST');
requireEnv('PORT');
requireEnv('AUTH_TOKEN');
requireEnv('USER_ID');


async function resolveRoom(rocketChatClient, roomName) {
    let room;
    if (roomName[0] === '#') {
        const result = await rocketChatClient.channels.list({});
        console.debug('channels', result);
        room = result.channels.find((room) => room.name === roomName.substring(1));
    }
    if (roomName[0] === '.') {
        const result = await rocketChatClient.groups.list({});
        console.debug('groups', result);
        room = result.groups.find((room) => room.name === roomName.substring(1));
    }
    return room;
}

async function post(roomName, text, alias, emoji) {
    const rocketChatClient = new RocketChatApi(
        process.env.PROTOCOL,
        process.env.HOST,
        process.env.PORT,
    )
    rocketChatClient.setAuthToken(process.env.AUTH_TOKEN);
    rocketChatClient.setUserId(process.env.USER_ID);
    console.info('RocketChat connected');
    const room = await resolveRoom(rocketChatClient, roomName);
    if (!room) {
        console.error('Room does not exist!');
        process.exit(1);
    }
    await rocketChatClient.chat.postMessage({
        roomId: room['_id'],
        text,
        alias,
        emoji
    });
    console.log('Looks like it\'s posted!');
    process.exit(0);
}

